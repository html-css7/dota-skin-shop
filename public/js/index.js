// faq dropdown
let dropdownMenuItems = document.querySelectorAll('.dropdown-menu__gradient')
let dropdownInner = document.querySelectorAll('.faq-dropdown .dropdown-inner')

for (let i = 0; i < dropdownMenuItems.length; i++) {
    dropdownMenuItems[i].addEventListener('click', () => {

        dropdownMenuItems.forEach(item => {
            item.classList.remove('active')
        })
        dropdownInner.forEach(item => {
            item.classList.remove('active')
        })

        dropdownMenuItems[i].classList.add('active')
        dropdownInner[i].classList.add('active')
    })   
}

// nav
let burger = document.getElementById('burger')
let headerNav =document.getElementById('header-nav')

burger.addEventListener('click', () => {
    if (!headerNav.classList.contains('active')) headerNav.classList.add('active')
    else headerNav.classList.remove('active')
})